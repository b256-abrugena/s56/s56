function countLetter(letter, sentence) {
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    let result = 0;

    if(letter !== "a" && letter !== "b" && letter !== "c" && letter !== "d" && letter !== "e" && letter !== "f" && letter !== "g" && letter !== "h" && letter !== "i" && letter !== "j" && letter !== "k" && letter !== "l" && letter !== "m" && letter !== "n" && letter !== "o" && letter !== "p" && letter !== "q" && letter !== "r" && letter !== "s" && letter !== "t" && letter !== "u" && letter !== "v" && letter !== "w" && letter !== "x" && letter !== "y" && letter !== "z" ){

        return undefined;

    }

    else{

        for(let i = 0; i < sentence.length; i++){
            if(sentence.charAt(i) == letter){
                result +=1;
            }
        }

        return result;
    }
    
}

console.log(countLetter('a','papaya'));


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    const downcasedText = text.toLowerCase();

    for(let a = 0; a < text.length; a++){
        for(let b = a + 1; b < text.length; b++){
            if(downcasedText[a] === downcasedText[b]){
                return false
            }
        }
    }

    return true;
    
}

console.log(isIsogram("polo"));


function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.

    if(age < 13){
        return undefined;
    }
    else if(age >= 13 && age <= 21){
        let newPrice1 = 0.80 * price;
        let adjustedPrice1 = newPrice1.toFixed(2);
        return String(adjustedPrice1);
    }
    else if(age > 21 && age < 60){
        let adjustedPrice2 = price.toFixed(2);
        return String(adjustedPrice2);
    }
    else if(age >= 60){
        let newPrice3 = 0.80 * price;
        let adjustedPrice3 = newPrice3.toFixed(2);
        return String(adjustedPrice3);
    }
}

let customer = purchase(17, 150);
console.log(customer);


function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

}



module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};
